<?php

$example_field = get_field( 'example_field' );

?>

<section class="<?= blockClasses( $block ) ?>" <?= blockSection( $block ) ?>>
	<div class="top-about">
		<div class="top-about__wrapper">
			<div class="container">
				<div class="top-about__inner">
					<InnerBlocks />
				</div>
			</div>
		</div>
	</div>
</section>
