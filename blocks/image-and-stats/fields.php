<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Headline", "text"],
        ["Photo", "image"],
        ["Tick", "image"],
        ["TextBox01", "text"],
        ["TextBox02", "text"],
        ["TextBox03", "text"],
        ["Paragraph", "text"]
    ]
);
