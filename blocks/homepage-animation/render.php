<?php
$title = get_field('title');
$title_tag = get_field('title_tag');
$title_tagger = 'h1';
$background_image = get_field('background_image');
$background_image_overlay = get_field('background_image_overlay');
if($title_tag){
  $title_tagger = $title_tag;
} else{
  $title_tagger = 'h1';
}
$content = get_field('content');
$cta = get_field('cta');
$background_image = get_field('background_image');
$background_image_overlay = get_field('background_image_overlay');

?>

<!-- ///////////////////// start acfblock-textandimageblocktwo /////////////////////////////////-->
<div class="acfblock-textandimageblocktwo <?php echo blockClasses($block) ?>" id="acfblock-textandimageblocktwo">
  <?php if ($background_image): ?>
    <figure class="background_a" style="background-image: url('<?php echo $background_image['url'] ?>')"></figure>
  <?php endif ?>
  <?php if ($background_image_overlay): ?>
    <figure class="background_b" style="background-image: url('<?php echo $background_image_overlay['url'] ?>')"></figure>
  <?php endif ?>
  <div class="containo_pemac">
    <div class="flexholder">
      <div class="leftarea">
        <?php if ($title): ?>
          <<?php echo $title_tagger ?> class="titletext"><?php echo $title; ?></<?php echo $title_tagger ?>>
        <?php endif ?>
        <?php if ($content): ?>
          <div class="contento">
            <?php echo $content; ?>
          </div>
        <?php endif ?>

        <?php if ($cta): ?>
          <a href="<?php echo $cta['url']; ?>" class="pemac_button outlined"><?php echo $cta['title']; ?></a>
        <?php endif ?>
      </div>
      <div class="imagearea" id="animationholder">
        <div class="animation_zone zone_1" id="animation_item_1"></div>
        <div class="animation_zone zone_2" id="animation_item_2"></div>
        <div class="animation_zone zone_3" id="animation_item_3"></div>
        <div class="animation_zone zone_4" id="animation_item_4"></div>
        <div class="animation_zone zone_5" id="animation_item_5"></div>
        <div class="animation_zone zone_6" id="animation_item_6"></div>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo get_stylesheet_directory_uri() ?>/blocks/homepage-animation/json/allitems.js"></script>
<script defer type="text/javascript">
var timerid;
var clearid;
  const target = document.getElementById('animationholder');
  var animationzone_1 = document.getElementById('animation_item_1');
  var animationzone_2 = document.getElementById('animation_item_2');
  var animationzone_3 = document.getElementById('animation_item_3');
  var animationzone_4 = document.getElementById('animation_item_4');
  var animationzone_5 = document.getElementById('animation_item_5');
  var animationzone_6 = document.getElementById('animation_item_6');
  var animationcontainer_1;
  var animationcontainer_2;
  var animationcontainer_3;
  var animationcontainer_4;
  var animationcontainer_5;
  var animationcontainer_6;
  var animo_1;
  var animo_2;
  var animo_3;
  var animo_4;
  var animo_5;
  var animo_6;
  var clearout;


  var animation_item_1 = function(){
    var animationData = allitems_animation_d;
    var honkparams = {
      container: animationzone_1,
      renderer: 'svg',
      loop: false,
      autoplay: false,
      animationData: animationData
    };

    animationcontainer_1 = lottie.loadAnimation(honkparams);
    animationcontainer_1.setSpeed(1.3);
    animationcontainer_1.play();
  };


  var animation_item_2 = function(){

    var animationData = allitems_animation_a;
    var honkparams = {
      container: animationzone_2,
      renderer: 'svg',
      loop: false,
      autoplay: false,
      animationData: animationData
    };

    animationcontainer_2 = lottie.loadAnimation(honkparams);
    animationcontainer_2.setSpeed(1.3);
    animationcontainer_2.play();
  };

  var animation_item_3 = function(){

    var animationData = allitems_animation_e;
    var honkparams = {
      container: animationzone_3,
      renderer: 'svg',
      loop: false,
      autoplay: false,
      animationData: animationData
    };

    animationcontainer_3 = lottie.loadAnimation(honkparams);
    animationcontainer_3.setSpeed(1.3);
    animationcontainer_3.play();
  };

  var animation_item_4 = function(){

    var animationData = allitems_animation_c;
    var honkparams = {
      container: animationzone_4,
      renderer: 'svg',
      loop: false,
      autoplay: false,
      animationData: animationData
    };

    animationcontainer_4 = lottie.loadAnimation(honkparams);
    animationcontainer_4.setSpeed(1.3);
    animationcontainer_4.play();
  };

  var animation_item_5 = function(){

    var animationData = allitems_animation_b;
    var honkparams = {
      container: animationzone_5,
      renderer: 'svg',
      loop: false,
      autoplay: false,
      animationData: animationData
    };

    animationcontainer_5 = lottie.loadAnimation(honkparams);
    animationcontainer_5.setSpeed(1.3);
    animationcontainer_5.play();
  };

  var animation_item_6 = function(){

    var animationData = allitems_animation_f;
    var honkparams = {
      container: animationzone_6,
      renderer: 'svg',
      loop: false,
      autoplay: false,
      animationData: animationData
    };

    animationcontainer_6 = lottie.loadAnimation(honkparams);
    animationcontainer_6.setSpeed(1.3);
    animationcontainer_6.play();
  };

  function emptyAll(){
    clearTimeout(animo_1);
    clearTimeout(animo_2);
    clearTimeout(animo_3);
    clearTimeout(animo_4);
    clearTimeout(animo_5);
    clearTimeout(animo_6);
    clearTimeout(clearout);
    animationzone_1.innerHTML = "";
    animationzone_2.innerHTML = "";
    animationzone_3.innerHTML = "";
    animationzone_4.innerHTML = "";
    animationzone_5.innerHTML = "";
    animationzone_6.innerHTML = "";
        // console.log('emptied');
  }

  var animationTimeline = function(){
    animationzone_1.innerHTML = "";
    animationzone_2.innerHTML = "";
    animationzone_3.innerHTML = "";
    animationzone_4.innerHTML = "";
    animationzone_5.innerHTML = "";
    animationzone_6.innerHTML = "";

    animo_1 = setTimeout(
      function(){ 
      animation_item_1();
      //console.log('animation 1');
      }, 
      200);


    animo_2 = setTimeout(
      function(){ 
      animation_item_2();
      //console.log('animation 2');
      }, 
      5000);


    animo_3 = setTimeout(
      function(){ 
      animation_item_3();
      //console.log('animation 3');
      }, 
      9000);

    animo_4 = setTimeout(
      function(){ 
      animation_item_4();
      //console.log('animation 4');
      }, 
      12750);

    animo_5 = setTimeout(
      function(){ 
      animation_item_5();
      //console.log('animation 5');
      }, 
      14250);

    animo_6 = setTimeout(
      function(){ 
      animation_item_6();
      //console.log('animation 6');
      }, 
      15750);

    clearout = setTimeout(
      function(){ 
      emptyAll();
      //console.log('clear');
      }, 
      19500);
  }

function runnAll(){
timerid = setInterval(function() {
  emptyAll();
    animationTimeline();
}, 21500);


//clear after 3
clearid = setTimeout(function(){
    emptyAll();
    clearInterval(timerid);
    //console.log('cleared pauser');
},129000);

// setTimeout(function(){ 
//       pauser(); 
//     }, 64000);


animationTimeline();
}
//end runnall


  let opts = {
    threshold: 0.5
  }

  var observer = new IntersectionObserver(function (entries) {
    if (!entries[0].isIntersecting) {
         emptyAll();
    clearInterval(timerid);
    clearInterval(clearid);
    }
    else {
runnAll();
    }
  }, opts);

  observer.observe(target)



// //reset if browser tab 
// document.addEventListener("visibilitychange", function() {
//    if (document.hidden){
//        //console.log("Browser tab is hidden")
//          emptyAll();
//     clearInterval(timerid);
//     clearInterval(clearid);
//    } else {
//        //console.log("Browser tab is visible")
// runnAll();
//    }
// });
//end





</script>
<!-- ///////////////////// break /////////////////////////////////-->

<style>
  .acfblock-textandimageblocktwo.homepageanimation{
    max-height: 50rem!important;
  }
  @media screen and (min-width: 1024px){
    .acfblock-textandimageblocktwo.homepageanimation{
      padding-top: 50px;
    }
  }
  .acfblock-textandimageblocktwo.homepageanimation{
    background: #002433!important;
  }
  .acfblock-textandimageblocktwo.homepageanimation .background_a{
    opacity: 1!important;
  }
  @media screen and  (min-width: 768px) and (max-width: 991px){
    .acfblock-textandimageblocktwo.homepageanimation h1.titletext {
      font-size: calc(2.75rem + .1vw - 20%)!important;
    }
  }
    @media screen and  (max-width: 767px){
    .acfblock-textandimageblocktwo.homepageanimation h1.titletext {
      font-size: calc(2.15rem + .1vw - 20%)!important;
    }
  }
</style>