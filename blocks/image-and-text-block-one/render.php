<?php
$image = get_field('image');
$position_image = get_field('image_position');
$link = get_field('link');
$background = get_field('background_color');
$title = get_field('title');
?>

<section class="one-image-text <?= blockClasses($block) ?>" <?= blockSection($block) ?> <?php if ($background): ?> style="background-color: <?php echo $background; ?> "<?php endif; ?>>
	<div class="one-image-text__wrapper">
		<div class="container">
			<?php if ($title): ?>
			<h2 style="text-align: center; margin-bottom: 30px"><?php echo $title?></h2>
			<?php endif; ?>
			<div class="one-image-text__inner <?php if ($position_image): ?> <?php echo $position_image; ?> <?php endif; ?>">
				<div class="one-image-text__content <?php if ($background == '#002433'): ?> <?php echo 'navy';?> <?php endif; ?>">
					<div class="one-image-text__teXt">
						<InnerBlocks/>
					</div>
					<?php if ($link):
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
						?>
						<div class="one-image-text__link main-link">
							<a href="<?php echo esc_url($link_url); ?>"
							   target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
						</div>
					<?php endif; ?>
				</div>
				<?php if ($image): ?>
					<div class="one-image-text__image">
						<?php echo wp_get_attachment_image($image['id'], 'full', false, array('class' => '')); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
