<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Shape", "image"],
        ["Headline", "text"],
        ["Title01", "text"],
        ["Photo01", "image"],
        ["Title02", "text"],
        ["Photo02", "image"],
        ["Title03", "text"],
        ["Photo03", "image"],
        ["Title04", "text"],
        ["Photo04", "image"],
        ["Title05", "text"],
        ["Photo05", "image"],
        ["Title06", "text"],
        ["Photo06", "image"],
        ["Title07", "text"],
        ["Photo07", "image"]
    ]
);

