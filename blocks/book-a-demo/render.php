<?php

$form = get_field('shortcode');
?>

<section class="<?= blockClasses($block) ?>" <?= blockSection($block) ?>>
	<div class="book-a-demo__wrapper">
		<div class="container">
			<div class="book-a-demo__inner">
				<div class="book-a-demo__left-side">
					<InnerBlocks/>
					<?php if (have_rows('list')): ?>
						<div class="book-a-demo__list">
							<?php while (have_rows('list')): the_row();
								$label = get_sub_field('title');
								$content = get_sub_field('content');
								?>
								<div class="book-a-demo__item">
									<?php if ($label): ?>
										<h5 class="book-a-demo__label">
											<?php echo $label; ?>
										</h5>
									<?php endif; ?>
									<?php if ($content): ?>
										<div class="book-a-demo__content">
											<?php echo $content; ?>
										</div>
									<?php endif; ?>
								</div>
							<?php endwhile; ?>
						</div>
					<?php endif; ?>
				</div>
				<?php if ($form): ?>
					<div class="book-a-demo__right-side">
						<div class="book-a-demo__form">
							<?php echo do_shortcode($form) ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
