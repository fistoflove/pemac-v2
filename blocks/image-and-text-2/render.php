<?php

$image = get_field('image');
$image_position = get_field('image_position');
$background = get_field('background_section');
?>

<section
		class="image-text-two <?= blockClasses($block) ?>" <?= blockSection($block) ?> <?php if ($background): ?> style="background-color: <?php echo $background; ?>" <?php endif; ?>>
	<div class="image-text-two__wrapper">
		<div class="container">
			<div class="image-text-two__inner <?php if ($image_position == 'right'): ?> reverse<?php endif; ?>">
				<?php if ($image): ?>
					<div class="image-text-two__image">
						<?php echo wp_get_attachment_image($image['id'], 'full', false, array('class' => '')); ?>
					</div>
				<?php endif; ?>
				<div class="image-text-two__content">
					<InnerBlocks/>
				</div>
			</div>
		</div>
	</div>
</section>
