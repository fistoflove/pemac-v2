<?php
$image = get_field('image');
$background = get_field('background_section');
?>

<section class="text-image-three <?= blockClasses($block) ?>" <?= blockSection($block) ?>>
	<div class="text-image-three__wrapper <?php if ($background == 'white' || $background == '#E3E0DE'): ?> <?php echo 'white'; ?> <?php endif; ?>" <?php if ($background): ?> style="background-color: <?php echo $background; ?> "<?php endif; ?>>
		<div class="container">
			<div class="text-image-three__inner">
				<InnerBlocks/>
				<div class="text-image-three__bullets">
					<?php if (have_rows('first_column_bullets')): ?>
						<div class="text-image-three-column">
							<ul class="text-image-three__lists">
								<?php while (have_rows('first_column_bullets')): the_row();
									$text_one = get_sub_field('item_first');
									?>
									<?php if ($text_one): ?>
										<li>
											<?php echo $text_one; ?>
										</li>
									<?php endif; ?>
								<?php endwhile; ?>
							</ul>
						</div>
					<?php endif; ?>
					<?php if (have_rows('second_column_bullets')): ?>
						<div class="text-image-three-column">
							<ul class="text-image-three__lists">
								<?php while (have_rows('second_column_bullets')): the_row();
									$text_two = get_sub_field('item_second');
									?>
									<?php if ($text_two): ?>
										<li>
											<?php echo $text_two ?>
										</li>
									<?php endif; ?>
								<?php endwhile; ?>
							</ul>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<?php if ($image): ?>
			<div class="text-image-three__image">
				<?php echo wp_get_attachment_image($image['id'], 'full', false, array('class' => '')); ?>
			</div>
		<?php endif; ?>
	</div>
</section>
