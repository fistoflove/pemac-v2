<?php
$link = get_field('custom_link');
$background = get_field('background');
$post_links = get_field('post_links');
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.css"/>
<section
		class="case-study <?= blockClasses($block) ?>" <?= blockSection($block) ?> <?php if ($background): ?> style="background-color: <?php echo $background ?> "<?php endif; ?>>
	<div class="case-study__wrapper">
		<div class="container">
			<div class="case-study__top">
				<InnerBlocks/>
			</div>
			<div class="case-study__bottom mySlider">
				<div class="case-study__inner swiper-wrapper">
					<?php
					$queried_object = get_queried_object();

					if ($queried_object) {
						$post_id = $queried_object->ID;
					}
					$my_posts = get_posts(array(
							'numberposts' => 4,
							'orderby' => 'date',
							'order' => 'DESC',
							'post_type' => 'case-study',
							'post__not_in' => array($post_id),
					));

					global $post;
					foreach ($my_posts as $post) {
						setup_postdata($post); ?>
						<div class="case-study__item swiper-slide">
							<div class="case-item match-height_case_study">
								<div class="case-item__content">
									<div class="case-item__content_inner">
										<h2 class="case-item__title">
											<?php the_title(); ?>
										</h2>
											<?php the_excerpt(); ?>
										<div class="case-item__link-container">
<!--											<div class="case-item__pagination">-->
<!--												<div class="swiper-pagination"></div>-->
<!--											</div>-->
											<?php if ($link):
												$link_url = $link['url'];
												$link_title = $link['title'];
												$link_target = $link['target'] ? $link['target'] : '_self';
												?>
												<div class="case-item__arrow main-link">
													<a  href="<?php echo esc_url($link_url); ?>"
													   target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
												</div>
											<?php endif; ?>
											
											<?php if ($post_links):
												$individual_link = get_the_permalink();
												?>
												<div class="case-item__arrow main-link">
													<a href="<?php echo esc_url($individual_link); ?>">Read More</a>
												</div>
											<?php endif; ?>
											
										</div>
									</div>
								</div>
								<?php if (has_post_thumbnail()): ?>
									<div class="case-item__image">
										<?php the_post_thumbnail(); ?>
									</div>
								<?php endif; ?>
							</div>
						</div>
					<?php }
					wp_reset_postdata(); ?>
				</div>
				<div class="swiper-pagination"></div>

			</div>
		</div>
	</div>
</section>
<script src="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.js"></script>
<script>
	var swiper = new Swiper(".mySlider", {
		effect: "fade",
		slidesPerView: 1,
		autoHeight: true,
		calculateHeight:true,
		pagination: {
			el: ".swiper-pagination",
			clickable: true,
		},
		autoplay: {
			delay: 5000,
		},
	});
	(function matchHeight() {
		//Grab divs with the class name 'match-height'
		var getDivs = document.getElementsByClassName('match-height_case_study');

		//Find out how my divs there are with the class 'match-height'
		var arrayLength = getDivs.length;
		var heights = [];

		//Create a loop that iterates through the getDivs variable and pushes the heights of the divs into an empty array
		for (var i = 0; i < arrayLength; i++) {
			heights.push(getDivs[i].offsetHeight);
		}

		//Find the largest of the divs
		function getHighest() {
			return Math.max(...heights);
		}

		//Set a variable equal to the tallest div
		var tallest = getHighest();

		//Iterate through getDivs and set all their height style equal to the tallest variable
		for (var i = 0; i < getDivs.length; i++) {
			getDivs[i].style.height = tallest + "px";
		}
	})();
</script>
