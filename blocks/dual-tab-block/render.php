<?php

$example_field = get_field('example_field');
$section_bg = get_field('section_background');

?>

<section class="section-tabs dual-tab <?= blockClasses($block) ?>" <?= blockSection($block) ?>>
	<?php if (have_rows('tab_items')):
		$k = 1;
		$l = 1;
		?>
		<div class="dual-tab__wrapper">
			<div class="container">
				<div class="dual-tab__top-content">
					<InnerBlocks/>
				</div>
				<div class="dual-tab__inner">
					<div class="dual-tab__labels">
						<?php while (have_rows('tab_items')): the_row();
							$label = get_sub_field('label'); ?>
							<h4 class="dual-tab__label tab" data-tab="tab<?php echo $k++ ?>">
								<?php echo $label; ?>
							</h4>
						<?php endwhile; ?>
					</div>
					<?php while (have_rows('tab_items')):
						the_row();
						$image = get_sub_field('image');
						$content = get_sub_field('content');
						$link = get_sub_field('link');
						?>
						<div class="dual-tab__tabs tab-pane" data-tab="tab<?php echo $l++ ?>">
							<div class="dual-tab__content dual-content">
								<?php if ($image): ?>
									<div class="dual-content__image">
										<?php echo wp_get_attachment_image($image['id'], 'full', false, array('class' => '')); ?>
									</div>
								<?php endif; ?>

								<div class="dual-content__content">
									<div class="dual-content__text">
										<?php if ($content): ?>
											<?php echo $content ?>
										<?php endif; ?>
										<?php if ($link):
											$link_url = $link['url'];
											$link_title = $link['title'];
											$link_target = $link['target'] ? $link['target'] : '_self';
											?>
											<div class="dual-content__link">
												<a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
											</div>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</section>
<script>
	function tabs() {
		const sections = Array.from(document.getElementsByClassName('section-tabs'));

		if (sections) {
			sections.forEach(section => {
				const tabs = Array.from(section.getElementsByClassName('tab'));
				const tabPanes = Array.from(section.getElementsByClassName('tab-pane'));

				tabs.forEach(tab => {
					tab.addEventListener('click', () => {
						const tabId = tab.getAttribute('data-tab');

						tabs.forEach(tab => {
							tab.classList.remove('active');
						});
						tab.classList.add('active');

						tabPanes.forEach(tabPane => {
							tabPane.style.display = 'none';
						});

						const activeTabPane = section.querySelector(`.tab-pane[data-tab="${tabId}"]`);
						activeTabPane.style.display = 'block';
					});
				});

				tabs[0].classList.add('active');
				const firstTabId = tabs[0].getAttribute('data-tab');
				const firstTabPane = section.querySelector(`.tab-pane[data-tab="${firstTabId}"]`);
				firstTabPane.style.display = 'block';
			});
		}
	}

	tabs();
</script>
