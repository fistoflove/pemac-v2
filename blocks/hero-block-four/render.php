<?php
$image = get_field('background_image');
?>

<section class="four-block <?= blockClasses($block) ?>" <?= blockSection($block) ?>>
	<div class="four-block__wrapper bg-cover" <?php bg($image['url']) ?> >
		<div class="container">
			<div class="four-block__inner">
				<?php if ($content = get_field('content')): ?>
					<div class="four-block__inner-block">
						<?php echo $content; ?>
					</div>
				<?php endif; ?>
				<div class="four-block__post-info">
					<div>
						<InnerBlocks/>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
