<?php

$bg = get_field('background_image');

?>

<section class="hero-block <?= blockClasses($block) ?>" <?= blockSection($block) ?>>
	<div class="hero-block__wrapper bg-cover" <?php bg($bg['url']) ?>>
		<div class="container">
			<div class="hero-block__inner">
				<div class="hero-block__text hero-block__item">
					<InnerBlocks/>
				</div>
				<div class="hero-block__right hero-block__item">
				</div>
			</div>
		</div>
	</div>
</section>
