<?php
$count_posts = get_field('posts_count');
$background = get_field('bg');
?>

<section
		class="news <?= blockClasses($block) ?>" <?= blockSection($block) ?> <?php if ($background): ?> style="background-color: <?php echo $background ?> "<?php endif; ?>>
	<div class="container">
		<div class="news__headings">
			<InnerBlocks/>
		</div>
		<div class="news__wrapper">
			<div class="news__inner">
				<?php $my_posts = get_posts(array(
						'numberposts' => 2,
						'category' => 0,
						'orderby' => 'date',
						'order' => 'DESC',
						'include' => array(),
						'exclude' => array(),
						'meta_key' => '',
						'meta_value' => '',
						'post_type' => 'post',
						'suppress_filters' => true,
				));

				global $post;

				foreach ($my_posts as $post) {
					setup_postdata($post);
					$post_categories = get_the_category();
					$category_names = array();

					foreach ($post_categories as $category) {
						$category_names[] = $category->name;
					}

					$category_list = implode(', ', $category_names);
					?>
					<div class="news__item">
						<div class="news-item">
							<?php if (has_post_thumbnail()): ?>
								<div class="news__img">
									<?php the_post_thumbnail(); ?>
								</div>
							<?php endif; ?>
							<div class="news__content">
								<h6 class="news__title">
									<?php echo $category_list; ?>
								</h6>
								<div class="news__excerpt">
									<a href="<?php the_permalink(); ?>" class="">
										<h3 class="news__excerpt-inner">
											<?php the_title(); ?>
										</h3>
									</a>
								</div>
							</div>
						</div>
					</div>
				<?php }
				?>
			</div>
			<?php wp_reset_postdata(); ?>
		</div>
	</div>
</section>
