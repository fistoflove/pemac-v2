<?php
$title = get_field('title');
$title_tag = get_field('title_tag');
$title_tagger = 'h1';
$background_image = get_field('background_image');
$background_image_position = get_field('background_image_position');
$background_image_overlay = get_field('background_image_overlay');
$background_image_overlay_position = get_field('background_image_overlay_position');
$align_content_style = get_field('align_content_style');
$gravity_form_shortcode = get_field('gravity_form_shortcode');
$push_if_top = get_field('push_if_top');

if ($title_tag) {
	$title_tagger = $title_tag;
} else {
	$title_tagger = 'h1';
}
$content = get_field('content');
$cta = get_field('cta');

?>

<!-- ///////////////////// start acfblock-textandimageblocktwo /////////////////////////////////-->
<div class="acfblock-textandimageblocktwo <?php echo blockClasses($block) ?>" id="acfblock-textandimageblocktwo">
	<?php if ($background_image): ?>
		<figure class="background_a <?php echo($background_image_position ? $background_image_position : '') ?>
		" style="background-image: url('<?php echo $background_image['url'] ?>')"></figure>
	<?php endif ?>
	<?php if ($background_image_overlay): ?>
		<figure class="background_b <?php echo($background_image_overlay_position ? $background_image_overlay_position : '') ?>"
				style="background-image: url('<?php echo $background_image_overlay['url'] ?>')"></figure>
	<?php endif ?>

	<div class="containo_pemac container <?php echo($push_if_top ? ' push_if_top ' : '') ?>">
		<div class="flexholder 	<?php echo($align_content_style ? $align_content_style : '') ?>">
			<div class="leftarea">
				<?php if ($title): ?>
				<<?php echo $title_tagger ?> class="titletext"><?php echo $title; ?></<?php echo $title_tagger ?>>
			<?php endif ?>
			<?php if ($content): ?>
				<div class="contento">
					<?php echo $content; ?>
				</div>
			<?php endif ?>

			<?php if ($cta): ?>
				<a href="<?php echo $cta['url']; ?>" class="pemac_button outlined"><?php echo $cta['title']; ?></a>
			<?php endif ?>
		</div>


		<?php if ($gravity_form_shortcode): ?>
			<div class="formarea">
				<div class="formholder">
					<?php echo do_shortcode($gravity_form_shortcode); ?>
				</div>
			</div>
		<?php endif ?>

	</div>
</div>
</div>
<!-- ///////////////////// end acfblock-textandimageblocktwo /////////////////////////////////-->

<!-- ///////////////////// break /////////////////////////////////-->
<script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>
<script>
	jQuery(document).ready(function($) {
		$('.gform_previous_button').wrap('<div class="your-custom-class"></div>');
	});
</script>

<script defer type="text/javascript">
	var backwardio = document.querySelectorAll("gform_previous_button");
	var forwardio = document.querySelectorAll("gform_next_button");
	var mainform = document.getElementsByClassName("gform_body");

	function performsStunt() {
		mainform[0].classList.add('hideo');
		setTimeout(function () {
			mainform[0].classList.remove('hideo');
		}, 1500);
	}


	function takesClick(thebutton) {
		thebutton.addEventListener("click", performsStunt);
	}

	function buttonSetup() {
		for (var i = 0; i < backwardio.length; i++) {
			takesClick(backwardio.item(i));
			console.log(backwardio.item(i));
		}

		for (var i = 0; i < forwardio.length; i++) {
			takesClick(forwardio.item(i));
		}
	}


	document.addEventListener("DOMContentLoaded", function () {
		buttonSetup();
	});


	var thebuttons = ['.gform_previous_button', '.gform_next_button'];
	document.addEventListener('click', function (event) {
		if (!event.target.matches(thebuttons)) return;
		performsStunt();
	}, false);
</script>


<!-- ///////////////////// break /////////////////////////////////-->
