<?php

$example_field = get_field( 'example_field' );

?>

<section class="privacy-policy <?= blockClasses( $block ) ?>" <?= blockSection( $block ) ?>>
    <div class="container">
		<div class="privacy-policy__content">
			<InnerBlocks />
		</div>
    </div>
</section>
