<?php

$background = get_field('background_color');
?>

<section
		class="cta-large <?= blockClasses($block) ?>" <?= blockSection($block) ?> <?php if ($background): ?> style="background-color: <?php echo $background ?> "<?php endif; ?>>
	<div class="container">
		<div class="cta-large__inner">
			<?php if ($content = get_field('content')): ?>
				<div class="cta-large__text">
					<div class="cta-large__content">
						<?php echo $content; ?>
					</div>
					<InnerBlocks/>
				</div>
			<?php endif; ?>
			<?php if ($image = get_field('background_image')): ?>
				<div class="cta-large__img">
					<?php echo wp_get_attachment_image($image['id'], 'full', false, array('class' => '')); ?>
				</div>
			<?php endif ?>
		</div>
	</div>
</section>

<script>
	window.addEventListener('load', function () {

		var target = document.querySelector('.cta-large__content');
		var appearBlock = document.querySelector('.cta-large__img img');
		var height_block = target.clientHeight  + 90;
		var appear_bllock_height = appearBlock.clientHeight;
		if(appear_bllock_height < height_block) {
			appearBlock.style.height = height_block  + 'px';
		}
	});
</script>
