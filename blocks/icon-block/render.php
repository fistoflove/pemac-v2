<?php
$background = get_field('background');
$border = get_field('border');
?>

<section
		class="block-icons <?= blockClasses($block) ?> <?php if ($background == '#002433'): ?> <?php echo 'navy'; ?> <?php endif; ?>" <?= blockSection($block) ?> <?php if ($background): ?> style="background-color: <?php echo $background; ?> "<?php endif; ?>>
	<div class="container">
		<div class="block-icons__wrapper" style="border-color: <?php echo $border; ?> ">
			<div class="block-icons__top-content text-center">
				<InnerBlocks/>
			</div>
			<?php if (have_rows('icons')): ?>
				<div class="block-icons__inner">
					<?php while (have_rows('icons')): the_row();
						$image = get_sub_field('icon');
						$descr = get_sub_field('description');
						?>
						<div class="item-icon">
							<div class="item-icon__wrapper">
								<?php if ($image): ?>
									<div class="item-icon__logo">
										<?php echo wp_get_attachment_image($image['id'], 'full', false, array('class' => '')); ?>
									</div>
								<?php endif; ?>
								<?php if ($descr): ?>
									<div class="item-icon__description">
										<?php echo $descr; ?>
									</div>
								<?php endif; ?>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>
