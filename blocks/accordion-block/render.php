<?php

$acc_position = get_field('accordion_position');
?>

<section class="accordion-block <?= blockClasses($block) ?>" <?= blockSection($block) ?>>
	<div class="accordion__wrapper">
		<div class="container">
			<div class="accordion__inner <?php if ($acc_position): ?>  order_<?php echo $acc_position;?> <?php endif; ?>">
				<div class="accordion__image">
					<InnerBlocks/>
				</div>
				<div class="accordion__accordion">
					<?php if (have_rows('accordion')): ?>
						<div class="accordion-items">
							<?php while (have_rows('accordion')): the_row();
								$label = get_sub_field('label');
								$content = get_sub_field('content')
								?>
								<div class="accordion-item">
									<?php if ($label): ?>
										<h3 class="accordion__label accordion">
											<span class="circle"></span>
											<?php echo $label; ?>
										</h3>
									<?php endif; ?>
									<?php if ($content): ?>
										<div class="accordion__content">
											<?php echo $content ?>
										</div>
									<?php endif; ?>
								</div>
							<?php endwhile; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>
