<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["ImageBackground", "image"],
        ["JobTitle", "text"],
        ["JobDescription", "text"],
        ["WorkPlace", "text"],
        ["WorkTime", "text"],
        ["ImageClock", "image"],
        ["ImagePlace", "image"]
    ]
);

