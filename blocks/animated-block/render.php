<?php

$box_content = get_field('box_content');
$link = get_field('link');
$background = get_field('background');

?>
<script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
<section
		class="animated-block <?= blockClasses($block) ?>" <?= blockSection($block) ?> <?php if ($background): ?> style="background-color: <?php echo $background; ?>" <?php endif; ?>>
	<div class="animated-block__wrapper">
		<div class="container">
			<div class="animated-block__inner <?php if ($background =='#002433'): ?> navy <?php endif; ?> ">
				<div class="animated-block__content">
					<div class="animated-block__text">
						<InnerBlocks/>
					</div>
					<?php if ($box_content): ?>
						<div class="animated-block__box">
							<?php echo $box_content; ?>
						</div>
					<?php endif; ?>
					<?php if ($link):
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
						?>
						<div class="animated-block__link main-link">
							<a href="<?php echo esc_url($link_url); ?>"
							   target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="animated-block__block">
			<lottie-player loop="true" autoplay="true"
						   src="<?php echo get_template_directory_uri() ?>/blocks/animated-block/Animation-PEMAC.json"
						   background="transparent" speed="1" style="width: 809px; height: 809px;"></lottie-player>
		</div>
	</div>
</section>
