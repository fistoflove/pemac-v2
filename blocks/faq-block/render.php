<?php
$background = get_field('section_background');
?>
<section
		class="faq-block <?= blockClasses($block) ?>" <?= blockSection($block) ?> <?php if ($background): ?> style="background-color: <?php echo $background; ?> "<?php endif; ?>>
	<div class="faq-block__wrapper">
		<div class="container">
			<div class="faq-block__inner <?php if ($background == "#002433"): ?> navy <?php endif; ?> <?php if ($background == "#E3E0DE"): ?> white <?php endif; ?>">
				<div class="faq-block__content">
					<InnerBlocks/>
				</div>
				<?php if (have_rows('faq_information')): ?>
					<div class="faq-block__accordion">
						<div class="faq-block__accordion-inner">
							<?php while (have_rows('faq_information')): the_row();
								$label = get_sub_field('label');
								$content = get_sub_field('content');
								$link = get_sub_field('link');
								?>
								<div class="faq-item">
									<?php if ($label): ?>
										<h3 class="faq-item__label accordion">
											<?php echo $label; ?>
											<span class="circle"></span>
										</h3>
									<?php endif; ?>
									<div class="faq-item__content">
										<?php if ($content): ?>
											<div class="faq-item__text">
												<?php echo $content; ?>
											</div>
										<?php endif; ?>
										<?php if ($link):
											$link_url = $link['url'];
											$link_title = $link['title'];
											$link_target = $link['target'] ? $link['target'] : '_self';
											?>
											<div class="faq-item__link main-link">
												<a href="<?php echo esc_url($link_url); ?>"
												   target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
											</div>
										<?php endif; ?>
									</div>
								</div>
							<?php endwhile; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>

		</div>
	</div>
</section>
