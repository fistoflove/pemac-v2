<?php

$example_field = get_field('example_field');
?>

<section class="case-study-hero <?= blockClasses($block) ?>" <?= blockSection($block) ?>>
	<div class="case-study-hero__wrapper">
		<div class="case-study-hero__decor">
			<img src="https://pemac-staging.imsmarketing.ie/wp-content/themes/pemac/blocks/job-hero/background.png"
				 alt="">
		</div>
		<div class="case-study-hero__container">
			<div class="case-study-hero__inner">
				<h4 class="case-study-hero__subtitle">POST</h4>
				<h1 class="case-study-hero__title">
					<?php the_title(); ?>
				</h1>
				<div class="case-study-hero__excerpt">
					<?php the_excerpt(); ?>
				</div>
				<?php if ($download_button = get_field('download_button', get_the_ID())): ?>
					<div class="case-study-hero__download">
						<a download="<?php echo $download_button['title']; ?>"
						   href="<?php echo $download_button['url']; ?>">
							Download
						</a>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
