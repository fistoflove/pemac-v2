<?php
$count_posts = get_field('posts_count');
$background = get_field('bg');

?>

<section
		class="nfeed <?= blockClasses($block) ?>" <?= blockSection($block) ?> <?php if ($background): ?> style="background-color: <?php echo $background ?> "<?php endif; ?>>
	<div class="container">
		<div class="nfeed__headings">
			<InnerBlocks/>
		</div>
		<div class="nfeed__wrapper">
			<div class="nfeed__inner">
				<?php $my_posts = get_posts(array(
						'numberposts' =>  $count_posts,
						'category' => 0,
						'orderby' => 'date',
						'order' => 'DESC',
						'include' => array(),
						'exclude' => array(),
						'meta_key' => '',
						'meta_value' => '',
						'post_type' => 'post',
						'suppress_filters' => true,
				));

				global $post;

				foreach ($my_posts as $post) {
					setup_postdata($post); ?>
					<div class="nfeed__item">
						<div class="nfeed-item">
							<?php if (has_post_thumbnail()): ?>
								<div class="nfeed__img">
									<?php the_post_thumbnail(); ?>
								</div>
							<?php endif; ?>
							<div class="nfeed__content">
								<h6 class="nfeed__title">
									<?php the_title(); ?>
								</h6>
								<div class="nfeed__excerpt">
									<div class="nfeed__excerpt-inner">
										<?php the_excerpt(); ?>
									</div>
									<div class="nfeed__arrow-right">
										<a href="<?php the_permalink(); ?>" class="">
											<img src="<?php echo get_template_directory_uri() ?>/assets/images/arrow-right_circle.svg"
												 alt="">
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php }
				?>
			</div>
			<?php wp_reset_postdata(); ?>
		</div>
	</div>
</section>
