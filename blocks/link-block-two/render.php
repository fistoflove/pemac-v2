<?php
$background = get_field('background');
?>

<section
		class="<?php if ($background): ?>  <?php echo $background ?> <?php endif; ?>  links-two <?= blockClasses($block) ?>" <?= blockSection($block) ?> <?php if ($background): ?> style="background-color: <?php echo $background ?> "<?php endif; ?>>
	<div class="link-two__text">
		<div class="container">
			<InnerBlocks/>
		</div>
	</div>
	<?php if (have_rows('links')): ?>
		<div class="link-two__links">
			<div class="container">
				<div class="links-two__wrapper">
					<?php while (have_rows('links')): the_row();
						$link = get_sub_field('link');
						if ($link):
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
							?>
							<div class="links-two__item">
								<a class="links-two-inn__link" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
									<div class="links-two__inn">
										<h6 class="links-two__name">
											<?php echo esc_html($link_title); ?>
										</h6>
										<div class="links-two__circle">
											<img src="<?php echo get_template_directory_uri() . '/assets/images/arrowBlack-right.svg' ?>"
												 alt="arrow-right">
										</div>
									</div>
								</a>
							</div>
						<?php endif; ?>
					<?php endwhile; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</section>
