<?php
$title = get_field('title');
$title_tag = get_field('title_tag');
$title_tagger = 'h1';
$background_image = get_field('background_image');
$background_image_overlay = get_field('background_image_overlay');
if($title_tag){
	$title_tagger = $title_tag;
} else{
	$title_tagger = 'h1';
}
$content = get_field('content');
$cta = get_field('cta');
$background_image = get_field('background_image');
$background_image_overlay = get_field('background_image_overlay');
$icon_a = get_field('icon_a');
$icon_b = get_field('icon_b');
$icon_c = get_field('icon_c');
?>

<!-- ///////////////////// start acfblock-textandimageblocktwo /////////////////////////////////-->
<div class="acfblock-textandimageblocktwo <?php echo blockClasses($block) ?>" id="acfblock-textandimageblocktwo">
	<?php if ($background_image): ?>
		<figure class="background_a" style="background-image: url('<?php echo $background_image['url'] ?>')"></figure>
	<?php endif ?>
	<?php if ($background_image_overlay): ?>
		<figure class="background_b" style="background-image: url('<?php echo $background_image_overlay['url'] ?>')"></figure>
	<?php endif ?>
	<div class="circlezone">
		<div class="circlebox circlebox_a">
			<figure class="maincircle rotating"></figure>
			<?php if ($icon_a): ?>
				<div class="blurbox boximage_a hasfader" id="faderid_1">
					<div class="underglass">
					</div>
					<img src="<?php echo $icon_a['url'] ?>" alt="<?php echo $icon_a['alt'] ?>">
				</div>
			<?php endif ?>
		</div>
		<div class="circlebox circlebox_b">
			<figure class="maincircle rotating_slower"></figure>

			<?php if ($icon_b): ?>
				<div class="blurbox boximage_ba hasfader" id="faderid_2">
					<div class="underglass">
					</div>
					<img src="<?php echo $icon_b['url'] ?>" alt="<?php echo $icon_b['alt'] ?>">
				</div>
			<?php endif ?>

			<?php if ($icon_c): ?>
				<div class="blurbox boximage_bb hasfader" id="faderid_3">
					<div class="underglass">
					</div>
					<img src="<?php echo $icon_c['url'] ?>" alt="<?php echo $icon_c['alt'] ?>">
				</div>
			<?php endif ?>
		</div>
	</div>

	<div class="containo_pemac">
		<div class="flexholder">
			<div class="leftarea">
				<InnerBlocks />
			</div>
		</div>
	</div>
</div>
<!-- ///////////////////// end acfblock-textandimageblocktwo /////////////////////////////////-->

<!-- ///////////////////// break /////////////////////////////////-->

<script defer type="text/javascript">
	function textAndImageBlockTwo(){
		var faders = document.getElementsByClassName('hasfader');
		for (const [key, value] of Object.entries(faders)) {
			setTimeout(function(){
				value.classList.add('fadedin');
			}, (400*key));
		}
	}


	document.addEventListener("DOMContentLoaded", function() {


		var textandimageblocktwoelement = document.getElementById('acfblock-textandimageblocktwo');

		if(textandimageblocktwoelement){
			textAndImageBlockTwo();
		}
	});

</script>
<!-- ///////////////////// break /////////////////////////////////-->
<style>
	@media screen and (max-width:767px ){
		.acfblock-textandimageblocktwo.hero-two{
			padding-top: 20px!important;
			min-height: 90vh!important;
			height: auto!important;
			padding-bottom: 15px!important;
		}
	}
	.acfblock-textandimageblocktwo.hero-two .leftarea{
		position: relative;
		z-index: 10;
	}
</style>
