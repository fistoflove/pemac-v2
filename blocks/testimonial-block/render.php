<?php

$content = get_field('content');
$client = get_field('client');
$image = get_field('image')

?>

<section class="testimonial <?= blockClasses($block) ?>" <?= blockSection($block) ?>>
	<div class="testimonial__wrapper">
		<div class="container">
			<div class="testimonial__inner">
				<div class="testimonial__intro">
					<InnerBlocks/>
				</div>
				<div class="testimonial__feedback_outher">
					<div class="testimonial__feedback">
						<?php if ($content): ?>
							<div class="testimonial__text">
								<?php echo $content; ?>
							</div>
						<?php endif; ?>
						<div class="testimonial__client-info">
							<?php if ($client): ?>
								<p class="testimonial__name">
									<?php echo $client; ?>
								</p>
							<?php endif; ?>
							<?php if ($image): ?>
								<div class="testimonial__logo">
									<?php echo wp_get_attachment_image($image['id'], 'full', false, array('class' => '')); ?>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
