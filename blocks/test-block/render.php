<?php

$example_field = get_field( 'example_field' );

?>

<section class="<?= blockClasses( $block ) ?>" <?= blockSection( $block ) ?>>
    <div class="container">
        <InnerBlocks />
    </div>
</section>
