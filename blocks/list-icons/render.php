<?php

$example_field = get_field('example_field');

?>

<div class="<?= blockClasses($block) ?>" <?= blockSection($block) ?>>
	<div class="container">
		<div class="list-icons__wrapper">
			<?php if (have_rows('list')): ?>
				<div class="list-icons__list">
					<?php while (have_rows('list')): the_row();
						$label = get_sub_field('label');
						$image = get_sub_field('image');
						?>
						<div class="list-icons__item">
							<?php if ($image): ?>
								<div class="list-icons__icon">
									<?php echo wp_get_attachment_image($image['id'], 'full', false, array('class' => '')); ?>
								</div>
							<?php endif; ?>
							<?php if ($label): ?>
								<p class="list-icons__label">
									<?php echo $label; ?>
								</p>
							<?php endif; ?>
						</div>
					<?php endwhile; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
