<?php
$image = get_field('image');
$alternative_layouts = get_field('alternative_layouts');
?>

<section class="hero-block-three alternative_layouts is_top_of_page <?= blockClasses($block) ?>" <?= blockSection($block) ?>>
	<div class="leftbean"></div>
	<div class="hero-block-three__wrapper">
		<div class="container">
			<div class="hero-block-three__inner">
				<InnerBlocks/>
			</div>
		</div>
		<?php if ($image): ?>
			<div class="hero-block-three__image">

				<figure class="altbackimage" style="background-image: linear-gradient(45deg,
              rgba(0,0,0, 0.75),
              rgba(0,0,0, 0.25)),  url(<?php echo $image['url']; ?>)">
					<img src="<?php echo $image['url']; ?>">
				</figure>
			</div>
		<?php endif; ?>
	</div>
</section>

<style>
	@media screen and (max-width: 767px){

	}
	.hero-block-three.alternative_layouts .hero-block-three__image figure.altbackimage{
		border-bottom-right-radius: 0!important;
	}
</style>
