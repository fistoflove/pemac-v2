<?php

$example_field = get_field('example_field');

?>

<section class="collapsable-content <?= blockClasses($block) ?>" <?= blockSection($block) ?>>
	<div class="collapsable-content__container">
		<div class="container">
			<InnerBlocks/>
		</div>
	</div>
	<?php if (have_rows('collapsable_content')): ?>
		<div class="collapsable-content__wrapper">
			<div class="container">
				<div class="collapsable-content__inner">
					<?php while (have_rows('collapsable_content')): the_row();
						$label = get_sub_field('label');
						$content = get_sub_field('content');
						?>
						<div class="collapsable-content__item accordion">
							<?php if ($label): ?>
								<h5 class="collapsable-content__label accordion__intro">
									<?php echo $label; ?>
									<span class="plus_minus">
									</span>
								</h5>
							<?php endif; ?>
							<?php if ($content): ?>
								<div class="collapsable-content__content accordion__content">
									<?php echo $content; ?>
								</div>
							<?php endif; ?>
						</div>
					<?php endwhile; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</section>
<script>

	function accordionchange() {
		const accordions = document.querySelectorAll(".accordion");

		const openAccordion = (accordion) => {
			const content = accordion.querySelector(".accordion__content");
			accordion.classList.add("accordion__active");
			content.style.maxHeight = content.scrollHeight + "px";
		};

		const closeAccordion = (accordion) => {
			const content = accordion.querySelector(".accordion__content");
			accordion.classList.remove("accordion__active");
			content.style.maxHeight = null;
		};

		accordions.forEach((accordion) => {
			const intro = accordion.querySelector(".accordion__intro");
			const content = accordion.querySelector(".accordion__content");

			intro.onclick = () => {
				if (content.style.maxHeight) {
					closeAccordion(accordion);
				} else {
					accordions.forEach((accordion) => closeAccordion(accordion));
					openAccordion(accordion);
				}
			};
		});
	}

	accordionchange();
</script>
