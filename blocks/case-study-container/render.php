<?php

$example_field = get_field( 'example_field' );

?>

<section class="<?= blockClasses( $block ) ?>" <?= blockSection( $block ) ?>>
    <div class="container_case-study">
        <InnerBlocks />
    </div>
</section>

