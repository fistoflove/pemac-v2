<?php

$image = get_field('image_small');
$text_bg = get_field('text_background');
$bg_section = get_field('section_background');
$reverse = get_field('image_position');

?>

<section
		class="small-cta <?= blockClasses($block) ?>" <?= blockSection($block) ?> <?php if ($bg_section): ?> style="background-color: <?php echo $bg_section; ?> "<?php endif; ?>>
	<div class="small-cta__wrapper">
		<div class="container">
			<div class="small-cta__inner image-position_<?php if ($reverse): ?><?php echo $reverse; ?> <?php endif; ?>">
				<div class="small-cta__text small-cta__column <?php if ($text_bg): ?> <?php echo $text_bg; ?> <?php endif; ?>" <?php if ($text_bg): ?> style="background-color: <?php echo $text_bg; ?> "<?php endif; ?>>
					<div class="small-cta__content">
						<InnerBlocks/>
					</div>
				</div>
				<?php if ($image): ?>
					<div class="small-cta__image">
						<?php echo wp_get_attachment_image($image['id'], 'full', false, array('class' => '')); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
