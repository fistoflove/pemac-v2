<?php

$images = get_field('logo_block');
$size = 'full';
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.css" />
<section class="logo-block <?= blockClasses($block) ?>" <?= blockSection($block) ?>>
	<div class="logo-block__wrapper">
		<div class="logo-block__title">
			<InnerBlocks/>
		</div>
		<div class="container">
			<div class="mySwiper logo-block__swiper">
				<?php if ($images): ?>
					<div class="swiper-wrapper slider-wrapper">
						<?php foreach ($images as $image_id): ?>
							<div class="logo-block__item swiper-slide">
								<?php echo wp_get_attachment_image($image_id, $size); ?>
							</div>
						<?php endforeach; ?>
					</div>
					<div class="swiper-button-next"></div>
					<div class="swiper-button-prev"></div>
					<div class="swiper-pagination"></div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
<script src="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.js"></script>
<script>
	var swiper = new Swiper(".mySwiper", {
		slidesPerView: 4,
		spaceBetween: 46,
		speed: 2000,
		autoplay: {
			delay: 3000,
			disableOnInteraction: false,
		},
		navigation: {
			nextEl: `.swiper-button-next`,
			prevEl: `.swiper-button-prev`,
		},
		breakpoints: {
			320: {
				slidesPerView: 1,
				spaceBetween: 20,
				slideToClickedSlide: true,
			},
			400: {
				slidesPerView: 2,
				slideToClickedSlide: true,
				spaceBetween: 14,
			},
			768: {
				slidesPerView: 3,
				spaceBetween: 51,
				slideToClickedSlide: true,
				pagination: {
					el: ".swiper-pagination",
					clickable: true,
				},
			},
			1024: {
				slidesPerView: 4,
				spaceBetween: 48,
				slideToClickedSlide: true,
			}
		}
	});
</script>
