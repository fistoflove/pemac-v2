<?php

$example_field = get_field('example_field');
$background = get_field('section_bg');
?>

<section
		class="linked-cards <?= blockClasses($block) ?>" <?= blockSection($block) ?> <?php if ($background): ?> style="background-color: <?php echo $background; ?> "<?php endif; ?>>
	<div class="linked-cards__wrapper">
		<div class="container">
			<div class="linked-cards__top-content text-center">
				<InnerBlocks/>
			</div>
			<?php if (have_rows('cards')): ?>
				<div class="linked-cards__inner">
					<?php while (have_rows('cards')): the_row();
						$content = get_sub_field('content');
						$link = get_sub_field('link');
						$image = get_sub_field('background_image');
						?>
						<div class="linked-cards__item">
							<?php if ($link):
								$link_url = $link['url'];
								$link_title = $link['title'];
								$link_target = $link['target'] ? $link['target'] : '_self';
								?>
								<a class="linked-cards__url" href="<?php echo esc_url($link_url); ?>"
								   target="<?php echo esc_attr($link_target); ?>">
									<div class="linked-item">
										<div class="linked-item__inner bg-cover" <?php bg($image['url']) ?>>
											<?php if ($content): ?>
												<div class="linked-item__content">
													<?php echo $content; ?>
												</div>
											<?php endif; ?>

											<div class="linked-item__link main-link">
												<p class="arrow-item"><?php echo esc_html($link_title); ?></p>
											</div>
										</div>
									</div>
								</a>
							<?php endif; ?>
						</div>
					<?php endwhile; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>
