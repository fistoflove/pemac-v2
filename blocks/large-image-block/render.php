<?php

$image = get_field('image');
$background = get_field('background_color');

?>

<section
		class="<?= blockClasses($block) ?>" <?= blockSection($block) ?> <?php if ($background): ?> style="background-color: <?php echo $background ?> "<?php endif; ?>>
	<div class="container large-image-block">
		<div class="large-image-block__inner text-center">
			<InnerBlocks/>
		</div>
		<?php if ($image):?>
		<div class="large-image-block__img text-center">
			<?php echo wp_get_attachment_image( $image['id'], 'full', false, array( 'class' => '' ) ); ?>
		</div>
		<?php endif; ?>
	</div>
</section>
