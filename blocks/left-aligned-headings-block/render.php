<?php

$link = get_field('link');
$background = get_field('background_section');

?>

<section class="left-aligned-heading <?= blockClasses($block) ?>" <?= blockSection($block) ?> <?php if ($background): ?> style="background-color: <?php echo $background ?> "<?php endif; ?>>
	<div class="left-aligned-heading__wrapper">
		<div class="container">
			<div class="left-aligned-heading__content <?php if ($background == '#002433'): ?> <?php echo 'navy' ?> <?php endif; ?>">
				<InnerBlocks/>
				<?php if ($link):
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<div class="left-aligned-heading main-link">
						<a href="<?php echo esc_url($link_url); ?>"
						   target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
