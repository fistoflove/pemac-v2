<?php
$image = get_field('image')

?>

<section class="hero-block-three <?= blockClasses($block) ?>" <?= blockSection($block) ?>>
	<div class="hero-block-three__wrapper">
		<div class="container">
			<div class="hero-block-three__inner">
				<InnerBlocks/>
			</div>
		</div>
		<?php if ($image): ?>
			<div class="hero-block-three__image">
				<?php echo wp_get_attachment_image($image['id'], 'full', false, array('class' => '')); ?>
			</div>
		<?php endif; ?>
	</div>
</section>
<style>
	@media screen and (min-width: 1024px) {
		.hero-block-three__inner {
			display: flex;
			align-items: center;
			padding-top: 200px;
			padding-bottom: 55px;
		}
	}
</style>
