<?php

$footer_logo = get_field('footer_logo', 'options');
$copyright = get_field('copyright');
$shortcode = get_field('shortcode', 'options');

?>

<footer class="<?= blockClasses($block) ?>" <?= blockSection($block) ?>>
	<div class="footer__wrapper">
		<div class="container">
			<div class="footer__inner">
				<div class="footer__logo-row hide-for-desk">
					<?php if ($footer_logo): ?>
						<div class="footer__logo">
							<?php echo wp_get_attachment_image($footer_logo['id'], 'full', false, array('class' => 'logo_img')); ?>
						</div>
					<?php endif; ?>
					<?php if (have_rows('additional_images', 'options')): ?>
						<div class="footer__logo--add">
							<?php while (have_rows('additional_images', 'options')): the_row();
								$image = get_sub_field('image');
								?>
								<?php if ($image): ?>
									<div class="footer__image--additionals">
										<?php echo wp_get_attachment_image($image['id'], 'full', false, array('class' => '')); ?>
									</div>
								<?php endif; ?>
							<?php endwhile; ?>
						</div>
					<?php endif; ?>
				</div>
				<div class="footer__logo-column footer__column mobile_responsive">
					<div class="footer__logo-row hide-for-mobile">
						<?php if ($footer_logo): ?>
							<div class="footer__logo">
								<?php echo wp_get_attachment_image($footer_logo['id'], 'full', false, array('class' => 'logo_img')); ?>
							</div>
						<?php endif; ?>
						<?php if (have_rows('additional_images', 'options')): ?>
							<div class="footer__logo--add">
								<?php while (have_rows('additional_images', 'options')): the_row();
									$image = get_sub_field('image');
									?>
									<?php if ($image): ?>
										<div class="footer__image--additionals">
											<?php echo wp_get_attachment_image($image['id'], 'full', false, array('class' => '')); ?>
										</div>
									<?php endif; ?>
								<?php endwhile; ?>
							</div>
						<?php endif; ?>
					</div>
					<?php if ($shortcode): ?>
						<div class="footer__form">
							<?php echo do_shortcode($shortcode); ?>
							<p class="additional-form">
								By subscribing, you agree to our Privacy Policy and provide consent to receive updates from PEMAC.
							</p>
						</div>
					<?php endif; ?>
				</div>
				<div class="footer__menu-column footer__column">
					<?php if (has_nav_menu('footer-primary-menu')): ?>
						<div class="footer__menu-second__wrapper">
							<h6 class="footer__menu-second--label accordion">
								<span class="circle"></span>
								Software & Services
							</h6>
							<div class="footer__menu-first-column">
								<?php wp_nav_menu(array(
										'theme_location' => 'footer-primary-menu',
										'menu_class' => 'footer__nav-menu',
										'depth' => 2,
								)); ?>
							</div>
							<?php if ($twitter = get_field('twitter_link', 'options')):
								$link_url = $twitter['url'];
								$link_title = $twitter['title'];
								$link_target = $twitter['target'] ? $twitter['target'] : '_self'
								?>
								<div class="footer__social footer__twit hide-for-mobile">
									<a class="buttonn" href="<?php echo esc_url($link_url); ?>"
									   target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
								</div>
							<?php endif; ?>
							<?php if ($linkedin = get_field('linkedin_link', 'options')):
								$link_url = $linkedin['url'];
								$link_title = $linkedin['title'];
								$link_target = $linkedin['target'] ? $linkedin['target'] : '_self'
								?>
								<div class="footer__social footer__linkedin hide-for-mobile">
									<a class="buttonn" href="<?php echo esc_url($link_url); ?>"
									   target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
								</div>
							<?php endif; ?>
							<?php if ($vimeo = get_field('vimeo_link', 'options')):
								$link_url = $vimeo['url'];
								$link_title = $vimeo['title'];
								$link_target = $vimeo['target'] ? $vimeo['target'] : '_self'
								?>
								<div class="footer__social footer__vimeo hide-for-mobile">
									<a class="buttonn" href="<?php echo esc_url($link_url); ?>"
									   target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
								</div>
							<?php endif; ?>
						</div>
					<?php endif; ?>
					<?php if (has_nav_menu('footer-secondary-menu')): ?>
					<div class="footer__menu-second__wrapper">
						<h6 class="footer__menu-second--label accordion">
							<span class="circle"></span>
							Company
						</h6>
						<div class="footer__menu-second-column">
							<?php wp_nav_menu(array(
									'theme_location' => 'footer-secondary-menu',
									'menu_class' => 'footer__nav-menu',
									'depth' => 2,
							)); ?>
						</div>
						<?php endif; ?>
<!--						--><?php //if ($linkedin = get_field('linkedin_link', 'options')):
//							$link_url = $linkedin['url'];
//							$link_title = $linkedin['title'];
//							$link_target = $linkedin['target'] ? $linkedin['target'] : '_self'
//							?>
<!--							<div class="footer__social footer__linkedin hide-for-mobile">-->
<!--								<a class="buttonn" href="--><?php //echo esc_url($link_url); ?><!--"-->
<!--								   target="--><?php //echo esc_attr($link_target); ?><!--">--><?php //echo esc_html($link_title); ?><!--</a>-->
<!--							</div>-->
<!--						--><?php //endif; ?>
					</div>
				</div>
				<h6 class="office-label hide-for-desk accordion">
					<span class="circle"></span>
					Our Addresses
				</h6>
				<?php if (have_rows('office_information', 'options')): ?>
					<div class="footer__office-column footer__column">
						<?php while (have_rows('office_information', 'options')): the_row();
							$label = get_sub_field('city_office');
							$content = get_sub_field('description');
							?>
							<div class="footer__office-column--item">
								<?php if ($label): ?>
									<h6 class="footer__office-column--label">
										<?php echo $label; ?>
									</h6>
								<?php endif; ?>
								<?php if ($content): ?>
									<div class="footer__office-column--content">
										<?php echo $content; ?>
									</div>
								<?php endif; ?>
							</div>
						<?php endwhile; ?>
					</div>
				<?php endif; ?>
				<div class="footer__social-links_mobile hide-for-desk">
					<?php if ($twitter = get_field('twitter_link', 'options')):
						$link_url = $twitter['url'];
						$link_title = $twitter['title'];
						$link_target = $twitter['target'] ? $twitter['target'] : '_self'
						?>
						<div class="footer__social footer__twit hide-for-desk">
							<a class="buttonn" href="<?php echo esc_url($link_url); ?>"
							   target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
						</div>
					<?php endif; ?>
					<?php if ($linkedin = get_field('linkedin_link', 'options')):
						$link_url = $linkedin['url'];
						$link_title = $linkedin['title'];
						$link_target = $linkedin['target'] ? $linkedin['target'] : '_self'
						?>
						<div class="footer__social footer__linkedin hide-for-desk">
							<a class="buttonn" href="<?php echo esc_url($link_url); ?>"
							   target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
						</div>
					<?php endif; ?>
					<?php if ($vimeo = get_field('vimeo_link', 'options')):
						$link_url = $vimeo['url'];
						$link_title = $vimeo['title'];
						$link_target = $vimeo['target'] ? $vimeo['target'] : '_self'
						?>
						<div class="footer__social footer__vimeo hide-for-desk">
							<a class="buttonn" href="<?php echo esc_url($link_url); ?>"
							   target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<?php if ($copyright = get_field('copyright', 'options')): ?>
			<div class="container">
				<div class="footer__copy">
					<?php echo $copyright ?>
				</div>
			</div>
		<?php endif; ?>
	</div>
</footer>
<script>
	let acc = document.querySelectorAll(".accordion");
	let i;

	for (i = 0; i < acc.length; i++) {
		acc[i].addEventListener("click", function () {
			this.classList.toggle("active");
			let panel = this.nextElementSibling;
			if (panel.style.maxHeight) {
				panel.style.maxHeight = null;
			} else {
				panel.style.maxHeight = panel.scrollHeight + "px";
			}
		});
	}
</script>
