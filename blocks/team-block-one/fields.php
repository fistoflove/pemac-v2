<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Items", "repeater", [
            ["Name", "text"],
            ["Title", "text"],
            ["Image", "image"],
            ["Paragraph01", "wysiwyg"],
        ]]
    ]
);

