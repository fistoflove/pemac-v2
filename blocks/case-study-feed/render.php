<?php
$count_posts = 3;
$background = get_field('bg');
$fields = get_fields();
$how_many_posts = get_field('how_many_posts');
?>

<section
		class="nsfeed <?= blockClasses($block) ?>" <?= blockSection($block) ?> <?php if ($background): ?> style="background-color: <?php echo $background ?> "<?php endif; ?>>
	<div class="container">
		<div class="nsfeed__headings">
			<div class="wrapper">
 				<InnerBlocks/>
				<!-- ///////////////////// conditional 5.10.23 /////////////////////////////////-->
				<?php if (!empty($fields['content']['link']['url'])): ?>
										<div class="nsfeed__link top">
					<a href="<?php if( !empty($fields['content']['link']['url']) ){ echo $fields['content']['link']['url']; } ?>" class="link dsp-c-r">
						<img src="<?= get_stylesheet_directory_uri(); ?>/blocks/job-feed/arrow.png" alt="arrow"
							 class="arrow">
						<p>
							<strong><?php if( !empty($fields['content']['link']['title']) ){ echo $fields['content']['link']['title']; } ?></strong>
						</p>
					</a>
				</div>
<?php endif ?>	
<!-- ///////////////////// end conditional 5.10.23 /////////////////////////////////-->
			</div>
		</div>
		<div class="nsfeed__wrapper">
			<div class="nsfeed__inner">
				<?php $my_posts = get_posts(array(
						'numberposts' => -1,
						'category' => 0,
						'orderby' => 'date',
						'order' => 'DESC',
						'include' => array(),
						'exclude' => array(),
						'meta_key' => '',
						'meta_value' => '',
						'post_type' => 'case-study',
						'suppress_filters' => true,
				));

				global $post;


				foreach ($my_posts as $post) {
					setup_postdata($post);
					$post_categories = get_the_category();
					$category_names = array();

					foreach ($post_categories as $category) {
						$category_names[] = $category->name;
					}

					$category_list = implode(', ', $category_names); ?>
					<div class="nsfeed__item">
						<div class="nsfeed-item">
							<?php if (has_post_thumbnail()): ?>
								<div class="nsfeed__img">
									<?php the_post_thumbnail(); ?>
								</div>
							<?php endif; ?>
							<div class="nsfeed__content">
								<h6 class="nsfeed__title">
									<?php echo $category_list; ?>
								</h6>
								<div class="nsfeed__excerpt">
									<a href="<?php the_permalink(); ?>" class="">
										<h3 class="nsfeed__excerpt-inner">
											<?php the_title(); ?>
										</h3>
									</a>
								</div>
							</div>
						</div>
					</div>
				<?php }
				?>
			</div>
			<?php wp_reset_postdata(); ?>
		</div>
<!-- ///////////////////// conditional 5.10.23 /////////////////////////////////-->
<?php if (!empty($fields['content']['link']['url'])): ?>
			<div class="nsfeed__link bot">
			<a href="<?php if( !empty($fields['content']['link']['url']) ){ echo $fields['content']['link']['url']; } ?>" class="link dsp-c-r">
				<img src="<?= get_stylesheet_directory_uri(); ?>/blocks/job-feed/arrow.png" alt="arrow" class="arrow">
				<p>
					<strong><?php if( !empty($fields['content']['link']['title']) ){ echo $fields['content']['link']['title']; } ?></strong>
				</p>
			</a>
		</div>
<?php endif ?>
<!-- ///////////////////// end conditional 5.10.23 /////////////////////////////////-->
	</div>
</section>
<style>
	.page-id-858 .nsfeed__link.top{
		display: none;
	}
</style>
