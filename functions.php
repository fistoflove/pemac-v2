<?php
/**
 * Child Starter functions and definitions
 *
 */
use Timber\Timber;

 add_action('wp_head', function() {
	?>
	<link rel="stylesheet" href="https://i.icomoon.io/public/32fdff22a3/PEMAC/style.css">
	<?php
});


add_action( 'wp_enqueue_scripts', function() {
    wp_enqueue_style('theme-override', get_stylesheet_directory_uri() . '/static/base.min.css', array(), '1.0.0', 'all');
});

add_action('wp_head', function() {
	Timber::render( 'core/favicons.twig');
});

register_post_type( 'case-study', [
	'label'                 => __( 'Case Study', 'ims_theme' ),
	'description'           => __( 'Case Studies', 'ims_theme' ),
	'labels'                => [
		'name'                  => _x( 'Case Studies', 'Post Type General Name', 'ims_theme' ),
		'singular_name'         => _x( 'Case Study', 'Post Type Singular Name', 'ims_theme' ),
		'menu_name'             => __( 'Case Studies', 'ims_theme' ),
		'name_admin_bar'        => __( 'Case Study', 'ims_theme' ),
		'archives'              => __( 'Case Study Archives', 'ims_theme' ),
		'attributes'            => __( 'Case Study Attributes', 'ims_theme' ),
		'parent_item_colon'     => __( 'Parent Item:', 'ims_theme' ),
		'all_items'             => __( 'All Case Studies', 'ims_theme' ),
		'add_new_item'          => __( 'Add Case Study', 'ims_theme' ),
		'add_new'               => __( 'Add Case Study', 'ims_theme' ),
		'new_item'              => __( 'New Case Study', 'ims_theme' ),
		'edit_item'             => __( 'Edit Case Study', 'ims_theme' ),
		'update_item'           => __( 'Update Case Study', 'ims_theme' ),
		'view_item'             => __( 'View Case Study', 'ims_theme' ),
		'view_items'            => __( 'View Case Studies', 'ims_theme' ),
		'search_items'          => __( 'Search Case Study', 'ims_theme' ),
		'not_found'             => __( 'Not found', 'ims_theme' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'ims_theme' ),
		'featured_image'        => __( 'Featured Image', 'ims_theme' ),
		'set_featured_image'    => __( 'Set featured image', 'ims_theme' ),
		'remove_featured_image' => __( 'Remove featured image', 'ims_theme' ),
		'use_featured_image'    => __( 'Use as featured image', 'ims_theme' ),
		'insert_into_item'      => __( 'Insert into Case Study', 'ims_theme' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Case Study', 'ims_theme' ),
		'items_list'            => __( 'Case Studies list', 'ims_theme' ),
		'items_list_navigation' => __( 'Case Studies list navigation', 'ims_theme' ),
		'filter_items_list'     => __( 'Filter Case Studies list', 'ims_theme' ),
	],
	'supports'              => [ 'title', 'editor', 'custom-fields', 'thumbnail', 'excerpt' ],
	'hierarchical'          => true,
	'public'                => true,
	'show_ui'               => true,
	'show_in_menu'          => true,
	'menu_position'         => 25,
	'menu_icon'             => 'dashicons-embed-post',
	'show_in_admin_bar'     => true,
	'show_in_nav_menus'     => false,
	'can_export'            => false,
	'has_archive'           => false,
	'exclude_from_search'   => false,
	'publicly_queryable'    => true,
	'rewrite'               =>	[
		'slug'                  => 'case-studies',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	],
	'capability_type'       => 'post',
	'show_in_rest'          => true,
]);

register_post_type( 'Job', [
	'label'                 => __( 'Job', 'ims_theme' ),
	'description'           => __( 'Jobs', 'ims_theme' ),
	'labels'                => [
		'name'                  => _x( 'Jobs', 'Post Type General Name', 'ims_theme' ),
		'singular_name'         => _x( 'Job', 'Post Type Singular Name', 'ims_theme' ),
		'menu_name'             => __( 'Careers', 'ims_theme' ),
		'name_admin_bar'        => __( 'Job', 'ims_theme' ),
		'archives'              => __( 'Job Archives', 'ims_theme' ),
		'attributes'            => __( 'Job Attributes', 'ims_theme' ),
		'parent_item_colon'     => __( 'Parent Item:', 'ims_theme' ),
		'all_items'             => __( 'All Jobs', 'ims_theme' ),
		'add_new_item'          => __( 'Add Job', 'ims_theme' ),
		'add_new'               => __( 'Add Job', 'ims_theme' ),
		'new_item'              => __( 'New Job', 'ims_theme' ),
		'edit_item'             => __( 'Edit Job', 'ims_theme' ),
		'update_item'           => __( 'Update Job', 'ims_theme' ),
		'view_item'             => __( 'View Job', 'ims_theme' ),
		'view_items'            => __( 'View Jobs', 'ims_theme' ),
		'search_items'          => __( 'Search Job', 'ims_theme' ),
		'not_found'             => __( 'Not found', 'ims_theme' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'ims_theme' ),
		'featured_image'        => __( 'Featured Image', 'ims_theme' ),
		'set_featured_image'    => __( 'Set featured image', 'ims_theme' ),
		'remove_featured_image' => __( 'Remove featured image', 'ims_theme' ),
		'use_featured_image'    => __( 'Use as featured image', 'ims_theme' ),
		'insert_into_item'      => __( 'Insert into Job', 'ims_theme' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Job', 'ims_theme' ),
		'items_list'            => __( 'Jobs list', 'ims_theme' ),
		'items_list_navigation' => __( 'Jobs list navigation', 'ims_theme' ),
		'filter_items_list'     => __( 'Filter Jobs list', 'ims_theme' ),
	],
	'supports'              => [ 'title', 'editor', 'custom-fields', 'excerpt' ],
	'hierarchical'          => true,
	'public'                => true,
	'show_ui'               => true,
	'show_in_menu'          => true,
	'menu_position'         => 25,
	'menu_icon'             => 'dashicons-tickets',
	'show_in_admin_bar'     => true,
	'show_in_nav_menus'     => false,
	'can_export'            => false,
	'has_archive'           => false,
	'exclude_from_search'   => false,
	'publicly_queryable'    => true,
	'rewrite'               => [
		'slug'                  => 'careers',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	],
	'capability_type'       => 'post',
	'show_in_rest'          => true,
]);

if ( function_exists( 'acf_add_options_page' ) ) {

	acf_add_options_page( array(
			'page_title' => 'Theme General Settings',
			'menu_title' => 'Theme Settings',
			'menu_slug'  => 'theme-general-settings',
			'capability' => 'edit_posts',
			'redirect'   => false,
	) );

}
function bg($img = '', $size = '', $echo = true)
{

	if (empty($img)) {
		return false;
	}

	if (is_array($img)) {
		$url = $size ? $img['sizes'][$size] : $img['url'];
	} else {
		$url = $img;
	}

	$string = 'style="background-image: url(' . $url . ')"';

	if ($echo) {
		echo $string;
	} else {
		return $string;
	}
}


/**
 * Stop gforms scrolling up on every page navigation next 15.09.23
 */
add_filter( 'gform_confirmation_anchor', '__return_false' );


/**
 * Register block script | homepage animation | Barry 07.09.23
 */

function lottie_include() {
wp_register_script( 'lottie_include', get_template_directory_uri() . '/blocks/homepage-animation/lottie.js', '');
}
add_action( 'init', 'lottie_include' );

/**
 * Register block script
 */


function custom_excerpt_length($length) {
	return 50;
}

function custom_excerpt_more($more) {
	return '...';
}

add_filter('excerpt_length', 'custom_excerpt_length');
add_filter('excerpt_more', 'custom_excerpt_more');
add_filter( 'gform_previous_button', 'my_previous_button_markup', 10, 2 );
function my_previous_button_markup( $previous_button, $form ) {

	$previous_button = '
<div class="prev_btn">' . $previous_button . '</div>
';

	return $previous_button;
}

 add_action('wp_head', function() {
	$user = wp_get_current_user();
	if($user->exists()) {
		?>
		<script>
		  window.markerConfig = {
			project: '65168e90570b9317e1239146',
			source: 'snippet',
			reporter: {
				email: '<?= $user->user_email; ?>',
				fullName: '<?= $user->display_name; ?>',
			},
		  };
		</script>
		<script>
		!function(e,r,a){if(!e.__Marker){e.__Marker={};var t=[],n={__cs:t};["show","hide","isVisible","capture","cancelCapture","unload","reload","isExtensionInstalled","setReporter","setCustomData","on","off"].forEach(function(e){n[e]=function(){var r=Array.prototype.slice.call(arguments);r.unshift(e),t.push(r)}}),e.Marker=n;var s=r.createElement("script");s.async=1,s.src="https://edge.marker.io/latest/shim.js";var i=r.getElementsByTagName("script")[0];i.parentNode.insertBefore(s,i)}}(window,document);
		</script>
		<?php
	}
  });